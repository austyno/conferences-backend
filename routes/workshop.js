const express = require('express');
const router = express.Router();
const Register = require('../models/workshop_reg.model');
const { WorkShopRegValidation } = require('../validation/validation');


//====== WORKSHOP REGISTRATION ENDPOINT =========
/**
 * @swagger
 * /workshop/register:
 *  post:
 *      description: User Registration for Workshops EndPoint
 *      responses:
 *          '200':
 *              description: Successful Registration
 *          '400':
 *              description: Bad Request
 *      tags: [Workshop]
 */

router.post(`/register`, async (req, res) => {
    // Validation
    const { error } = WorkShopRegValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    // Check if he/she has registered for the conference before
    const emailExists = await Register.findOne({ $and: [ { workshop_id: req.body.workshop_id }, { email: req.body.email } ] });
    if(emailExists) return res.status(400).send('You have registered for this conference already!');

    const reg = new Register({
                               workshop_name: req.body.workshop_name,
                               workshop_id: req.body.workshop_id,
                               location: req.body.location,
                               name: req.body.name,
                               email: req.body.email,
                               phone: req.body.phone
                            });

    // Save the Registration Details
    try {
        const regData = await reg.save();
        res.status(200).json({msg: 'Registration successful!'});
    } catch (error) {
        res.status(400).json({err: error, errMsg: 'Could not register for some reasons!',})
    }
});


module.exports = router;