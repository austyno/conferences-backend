const express = require('express');
const router = express.Router();
const Workshops = require('../../models/admin/workshop_creation.model');
const verify = require('../../token/verify_token');
const { WorkshopValidation, WorkshopEditValidation } = require('../../validation/validation');


//============================ Create a Workshop EndPoint ======================
/**
 * @swagger
 * /admin/workshop/create:
 *  post:
 *      description: Create a New Workshop
 *      responses:
 *          '200':
 *              description: Workshop Created Successfully
 *          '400':
 *              description: Bad Request
 *          '409':
 *              description: Conflict, server couldn’t process your browser’s request because there’s a conflict with the relevant resource
 *      tags: [Admin - Workshop]
 * 
 */
router.post('/create', verify, async (req, res) => {
    // Validation
    const { error } = WorkshopValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    // Check if the Workshop has been created before
    const workshopExists = await Workshops.findOne({_id: req.body.workshop_id});
    if(workshopExists) return res.status(400).send('This Workshop Already Exists')

    const workshop = new Workshops({
                            workshop_name: req.body.workshop_name,
                            location: req.body.location,
                            description: req.body.description,
                            start_date: req.body.start_date,
                            end_date: req.body.end_date
                        });

    // Save the Workshop to DB
    try {
        const workshopData = await workshop.save();
        res.status(200).json({msg: 'Workshop Created'});
    } catch (error) {
        res.status(409).json({err: error, errMsg: "Could not Create Workshop for some Reasons"});
    }
});



// ====================== GET ALL WORKSHOPS ================================
/**
 * @swagger
 * /admin/workshop/all:
 *  get:
 *      description: Get all Workshops
 *      responses:
 *          '200':
 *              description: Successful but without Content
 *          '201':
 *              description: Successful with Content
 *          '400':
 *              description: Bad Request
 *          '409':
 *              description: Conflict, server couldn’t process your browser’s request because there’s a conflict with the relevant resource
 *      tags: [Admin - Workshop]
 */
router.get(`/all`, async (req, res) => {
    // Get all workshops
    try {
        const allWorkshop = await Workshops.find();
        if(!allWorkshop) return res.status(200).send('No Workshop is available now');

        res.status(201).json(allWorkshop);
    } catch (error) {
        res.status(409).json({err: error, errMsg: "Could not Get all Workshops for some Reasons"});
    }
    
});



// ====== EDIT A WORKSHOP ===================
/**
 * @swagger
 * /admin/workshop/update/:id:
 *  put:
 *      description: Edit a Workshop
 *      responses:
 *          '201':
 *              description: Update Successful
 *          '400':
 *              description: Bad Request
 *          '404':
 *              description: Document to be modified was not found
 *          '409':
 *              description: Conflict, server couldn’t process your browser’s request because there’s a conflict with the relevant resource
 *      tags: [Admin - Workshop]
 */
router.put(`/update/:id`, verify, async (req, res) => {
    // Validation
    const { error } = WorkshopEditValidation(req.body);
    if(error) return res.status(400).json(error.details[0].message);

    // Try to Update the Workshop
    try {
        const editData = await Workshops.updateOne(
                            {_id: req.params.id}, 
                            {
                                workshop_name: req.body.workshop_name,
                                location: req.body.location,
                                description: req.body.description,
                                start_date: req.body.start_date,
                                end_date: req.body.end_date
                            }
                        );
        if(editData.nModified != 1) return res.status(404).send('Document not found');
        res.status(201).json(editData);
    } catch (error) {
        res.status(409).json({err: error, errMsg: "Could not Update for some Unknown Reasons"});
    }
});



// ====== DELETE A WORKSHOP ==============
/**
 * @swagger
 * /admin/workshop/delete/:id:
 *  delete:
 *      description: Delete a Workshop
 *      responses:
 *          '200':
 *              description: Successful Deletion
 *          '404':
 *              description: Document to be Deleted not Found
 *          '409':
 *              description: Conflict, server couldn’t process your browser’s request because there’s a conflict with the relevant resource
 *      tags: [Admin - Workshop]
 */
router.delete(`/delete/:id`, verify, async (req, res) => {
    // Check if Workshop exists
    const workshopExists = await Workshops.findOne({_id: req.params.id});
    if(!workshopExists) return res.status(404).send('Workshop was not Found');

    // Remove or Delete the workshop from the DB
    try {
        const removed = await Workshops.remove({_id: req.params.id});
        res.status(200).json({msg: "Workshop Deleted Succesfully!"});
    } catch (error) {
        res.status(409).json({err: error, errMsg: "Deletion not Successfull"});
    }
});


module.exports = router;