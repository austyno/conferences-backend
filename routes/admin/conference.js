const express = require('express');
const router = express.Router();
const Conferences = require('../../models/admin/conf_creation.model');
const verify = require('../../token/verify_token');
const { ConfValidation, ConfEditValidation } = require('../../validation/validation');

//===== Create a Conference EndPoint =====
/**
 * @swagger
 * /admin/conference/create:
 *  post:
 *      description: Create a New Conference
 *      responses:
 *          '200':
 *              description: Conference Created Successfully
 *          '400':
 *              description: Bad Request
 *          '409':
 *              description: Conflict, server couldn’t process your browser’s request because there’s a conflict with the relevant resource
 *      tags: [Admin - Conference]
 * 
 */

// body: {
//     "description": "conference",
//     "content": {
//         "text/plain": {
//             "schema": {
//                 "type": "array",
//                 "items": {
//                     "type": "string"
//             }
//         }
//     }
// }
// }
router.post('/create', verify, async (req, res) => {
    // Validation
    const { error } = ConfValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    // Check if the Conference has been created before
    const confExists = await Conferences.findOne({_id: req.body.conference_id});
    if(confExists) return res.status(400).send('This Conference Already Exists')

    const conf = new Conferences({
                            conference_name: req.body.conference_name,
                            location: req.body.location,
                            description: req.body.description,
                            start_date: req.body.start_date,
                            end_date: req.body.end_date
                        });

    // Save the Conference to DB
    try {
        const confData = await conf.save();
        res.status(200).json({msg: 'Conference Created'});
    } catch (error) {
        res.status(409).json({err: error, errMsg: "Could not Create Conference for some Reasons"});
    }
});


// ====== GET ALL CONFERENCES
/**
 * @swagger
 * /admin/conference/all:
 *  get:
 *      description: Get all Conferences
 *      responses:
 *          '200':
 *              description: Successful but without Content
 *          '201':
 *              description: Successful with Content
 *          '400':
 *              description: Bad Request
 *          '409':
 *              description: Conflict, server couldn’t process your browser’s request because there’s a conflict with the relevant resource
 *      tags: [Admin - Conference]
 */
router.get(`/all`, async (req, res) => {
    // Get all conferences
    try {
        const allConf = await Conferences.find();
        if(!allConf) return res.status(200).send('No conference is available now');

        res.status(201).json(allConf);
    } catch (error) {
        res.status(409).json({err: error, errMsg: "Could not Get all Conferences for some Reasons"});
    }
    
});



// ====== EDIT A CONFERENCE ===================
/**
 * @swagger
 * /admin/conference/update/:id:
 *  put:
 *      description: Edit a Conference
 *      responses:
 *          '201':
 *              description: Update Successful
 *          '400':
 *              description: Bad Request
 *          '404':
 *              description: Document to be modified was not found
 *          '409':
 *              description: Conflict, server couldn’t process your browser’s request because there’s a conflict with the relevant resource
 *      tags: [Admin - Conference]
 */
router.put(`/update/:id`, verify, async (req, res) => {
    // Validation
    const { error } = ConfEditValidation(req.body);
    if(error) return res.status(400).json(error.details[0].message);

    // Try to Update the Conference
    try {
        const editData = await Conferences.updateOne(
                            {_id: req.params.id}, 
                            {
                                conference_name: req.body.conference_name,
                                location: req.body.location,
                                description: req.body.description,
                                start_date: req.body.start_date,
                                end_date: req.body.end_date
                            }
                        );
        if(editData.nModified != 1) return res.status(404).send('Document not found');
        res.status(201).json(editData);
    } catch (error) {
        res.status(409).json({err: error, errMsg: "Could not Update for some Unknown Reasons"});
    }
});





// ====== DELETE A CONFERENCE ==============
/**
 * @swagger
 * /admin/conference/delete/:id:
 *  delete:
 *      description: Delete a conference
 *      responses:
 *          '200':
 *              description: Successful Deletion
 *          '404':
 *              description: Document to be Deleted not Found
 *          '409':
 *              description: Conflict, server couldn’t process your browser’s request because there’s a conflict with the relevant resource
 *      tags: [Admin - Conference]
 */
router.delete(`/delete/:id`, verify, async (req, res) => {
    // Check if Conference exists
    const confExists = await Conferences.findOne({_id: req.params.id});
    if(!confExists) return res.status(404).send('Conference was not Found');

    // Remove or Delete the conference from the DB
    try {
        const removed = await Conferences.remove({_id: req.params.id});
        res.status(200).json({msg: "Conference Deleted Succesfully!"});
    } catch (error) {
        res.status(409).json({err: error, errMsg: "Deletion not Successfull"});
    }
});


module.exports = router;