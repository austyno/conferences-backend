const express = require('express');
const router = express.Router();
const Register = require('../models/conf_registration.model');
const { ConfRegValidation } = require('../validation/validation');


//====== CONFERENCE REGISTRATION ENDPOINT =========
/**
 * @swagger
 * /conference/register:
 *  post:
 *      description: User Registration for Conferences EndPoint
 *      responses:
 *          '200':
 *              description: Successful Registration
 *          '400':
 *              description: Bad Request
 *      tags: [Conference]
 */
router.post(`/register`, async (req, res) => {
    // Validation
    const { error } = ConfRegValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    // Check if he/she has registered for the conference before
    const emailExists = await Register.findOne({ $and: [ { conference_id: req.body.conference_id }, { email: req.body.email } ] });
    if(emailExists) return res.status(400).send('You have registered for this conference already!');

    const reg = new Register({
                               conference_name: req.body.conference_name,
                               conference_id: req.body.conference_id,
                               location: req.body.location,
                               name: req.body.name,
                               email: req.body.email,
                               phone: req.body.phone
                            });

    // Save the Registration Details
    try {
        const regData = await reg.save();
        res.status(200).json({msg: 'Registration successful!'});
    } catch (error) {
        res.status(400).json({err: error, errMsg: 'Could not register for some reasons!',})
    }
});


module.exports = router;