const mongoose = require('mongoose');

const conferenceSchema = mongoose.Schema({
    conference_name: {
        type: String,
        required: true
    },

    location: {
        type: Array,
        required: true
    },

    description: {
        type: String,
        required: true
    },

    start_date: {
        type: String,
        required: true
    },

    end_date: {
        type: String,
        required: true
    }
},{
    timestamps: true,
});

module.exports = mongoose.model('Conference', conferenceSchema);