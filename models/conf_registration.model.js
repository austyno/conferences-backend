const mongoose = require('mongoose');

const registerSchema = mongoose.Schema({
    conference_id: {
        type: String,
        required: true
    },

    conference_name: {
        type: String,
        required: true
    },

    location: {
        type: Array,
        required: true
    },

    name: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true
    },

    phone: {
        type: Number,
        required: true
    }
},{
    timestamps: true,
});

module.exports = mongoose.model('Conference_Reg', registerSchema);