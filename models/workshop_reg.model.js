const mongoose = require('mongoose');

const registerSchema = mongoose.Schema({
    workshop_id: {
        type: String,
        required: true
    },

    workshop_name: {
        type: String,
        required: true
    },

    location: {
        type: Array,
        required: true
    },

    name: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true
    },

    phone: {
        type: Number,
        required: true
    }
},{
    timestamps: true,
});

module.exports = mongoose.model('Workshop_Reg', registerSchema);