const express = require('express');
const app = express();
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
require('dotenv/config')

// == Extended from: https://swagger.io/specification/#infoObject
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: "Conferences & Workshops",
            description: "Conferences & Workshop Backend/EndPoints",
            servers: ["http://localhost:8000"]
        }
    },
    apis: ["./routes/*.js", "./routes/admin/*.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// ======== Middlewares: Make node use Body Parser to Parse body of requests ========
app.use(bodyParser.json());
app.use(cors());

// =============== Import Middlewares and Use it =============
// =============== Import Middleware Routes ====
const confRoutes = require('./routes/conference');
const adminConferenceRoutes = require('./routes/admin/conference');
const adminWorkshopRoutes = require('./routes/admin/workshop');
const workshopRoutes = require('./routes/workshop');

// ======= Use the Middleware Route ======
app.use('/conference', confRoutes);
app.use('/admin/conference', adminConferenceRoutes);
app.use('/admin/workshop', adminWorkshopRoutes);
app.use('/workshop', workshopRoutes);


//=============== Main App Route ==============
// app.get('/', (req, res) => {
//     res.send('Home Route');
// });

//============= Connect to DB =========================
mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, () => {
    console.log('MongoDB connected!');
});


//======= Tell App to listen on port
app.listen(8000, () => {
    console.log('Server Running on 8000');
});