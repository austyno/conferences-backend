const Joi = require('@hapi/joi');

// Conference Registration Validation
const ConfRegValidation = (data) => {
    const schema = Joi.object({
        conference_id: Joi.string().required(),
        conference_name: Joi.string().required(),
        location: Joi.array().required(),
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        // min() does not work on numbers, the limit check has to be done from the frontend
        phone: Joi.number().required(), 
    });

    return schema.validate(data);
};

// Conference Creation Validation
const ConfValidation = (data) => {
    const schema = Joi.object({
        conference_name: Joi.string().required(),
        location: Joi.array().required(),
        description: Joi.string().required(),
        start_date: Joi.date().required(),
        end_date: Joi.date().required(),
    });

    return schema.validate(data);
};

// Conference Editing Validation
const ConfEditValidation = (data) => {
    const schema = Joi.object({ 
        conference_name: Joi.string().required(),
        location: Joi.array().required(),
        description: Joi.string().required(),
        start_date: Joi.date().required(),
        end_date: Joi.date().required(),
     });

     return schema.validate(data);
};





// =========================================== WORKSHOPS VALIDATION ==========================================================================
// Workshop Registration Validation
const WorkShopRegValidation = (data) => {
    const schema = Joi.object({
        workshop_id: Joi.string().required(),
        workshop_name: Joi.string().required(),
        location: Joi.array().required(),
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        // min() does not work on numbers, the limit check has to be done from the frontend
        phone: Joi.number().required(), 
    });

    return schema.validate(data);
};


// Workshop Creation Validation
const WorkshopValidation = (data) => {
    const schema = Joi.object({
        workshop_name: Joi.string().required(),
        location: Joi.array().required(),
        description: Joi.string().required(),
        start_date: Joi.date().required(),
        end_date: Joi.date().required(),
    });

    return schema.validate(data);
};


// Workshop Editing Validation
const WorkshopEditValidation = (data) => {
    const schema = Joi.object({ 
        workshop_name: Joi.string().required(),
        location: Joi.array().required(),
        description: Joi.string().required(),
        start_date: Joi.date().required(),
        end_date: Joi.date().required(),
     });

     return schema.validate(data);
};


//========== Export all functions ============
module.exports.ConfRegValidation = ConfRegValidation;
module.exports.ConfValidation = ConfValidation;
module.exports.ConfEditValidation = ConfEditValidation;
module.exports.WorkShopRegValidation = WorkShopRegValidation;
module.exports.WorkshopValidation = WorkshopValidation;
module.exports.WorkshopEditValidation = WorkshopEditValidation;